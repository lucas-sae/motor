<?php
declare(strict_types=1);

require_once '../app/vendor/autoload.php';

use League\Route\RouteGroup;
use MotorCheck\App\App;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

$dotenv = Dotenv\Dotenv::create('./../app');
$dotenv->load();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);

$router = new League\Route\Router;

$router->map('GET', '/', function (ServerRequestInterface $request): ResponseInterface {
    $response = new HtmlResponse(App::twig()->render('default.html.twig'));

    return $response;
});

$router->group('/github', function (RouteGroup $route) {
    $route->map('GET', '/', '\MotorCheck\App\Controllers\GitHubController::index');
    $route->map('GET', '/list', '\MotorCheck\App\Controllers\GitHubController::listRepositories');
    $route->map('GET', '/fetchFromApi', '\MotorCheck\App\Controllers\GitHubController::fetchFromApi');
    $route->map('GET', '/exportToCsv', '\MotorCheck\App\Controllers\GitHubController::exportToCsv');
});

$router->group('/report', function (RouteGroup $route) {
    $route->map('GET', '/', '\MotorCheck\App\Controllers\ReportController::index');
    $route->map('GET', '/list', '\MotorCheck\App\Controllers\ReportController::listReportData');
    $route->map('GET', '/exportToCsv', '\MotorCheck\App\Controllers\ReportController::downloadReport');
});


$response = $router->dispatch($request);

// send the response to the browser
(new Zend\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);