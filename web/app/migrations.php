<?php

return [
    'name' => 'Migrations',
    'migrations_namespace' => 'MotorCheck\App\Migrations',
    'table_name' => 'migration_versions',
    'column_name' => 'version',
    'column_length' => 14,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory' => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Migrations',
    'all_or_nothing' => true,
];