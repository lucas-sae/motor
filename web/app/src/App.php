<?php


namespace MotorCheck\App;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use GuzzleHttp\Client;
use League\Container\Container;
use League\Container\ReflectionContainer;
use PDO;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class App
{
    /**
     * @var Container
     */
    private static $container;

    /**
     * @return Container
     */
    public function getContainer()
    {
        if (!self::$container instanceof Container) {
            self::$container = new Container();
            self::$container->delegate(new ReflectionContainer());
        }

        return self::$container;
    }

    public static function make($class)
    {
        return self::getContainer($class);
    }

    /**
     * @return Environment
     */
    public static function twig()
    {
        $loader = new FilesystemLoader(__DIR__ . DIRECTORY_SEPARATOR . 'views');
        $twig = new Environment($loader);

        return $twig;
    }

    public static function em()
    {
        $config = Setup::createAnnotationMetadataConfiguration(
            [__DIR__ . "/src/Entities"],
            getenv('ENVIRONMENT') === 'dev'
        );

        $conn = [
            'host' => getenv('DB_HOSTNAME'),
            'dbname' => getenv('DB_DATABASE'),
            'user' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'driver' => 'pdo_mysql',
        ];

        return EntityManager::create($conn, $config);
    }

    /**
     * @return Client
     */
    public static function client()
    {
        return new Client([
            'base_uri' => 'https://api.github.com/users/',
            'connect_timeout' => 5,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
    }

    public static function pdo()
    {
        $dsn = sprintf('mysql:host=%s;dbname=%s', getenv('DB_HOSTNAME'), getenv('DB_DATABASE'));

        $dbh = new PDO(
            $dsn,
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD'),
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
        );
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $dbh;
    }
}
