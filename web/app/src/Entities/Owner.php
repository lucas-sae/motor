<?php
declare(strict_types=1);

namespace MotorCheck\App\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="owner")
 */
class Owner
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string", nullable=false) */
    private $name;

    /**
     * @OneToMany(targetEntity="Repository", mappedBy="owner")
     */
    private $repositories;

    public function __construct() {
        $this->repositories = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRepositories()
    {
        return $this->repositories;
    }

    /**
     * @param mixed $repositories
     */
    public function setRepositories($repositories): void
    {
        $this->repositories = $repositories;
    }
}