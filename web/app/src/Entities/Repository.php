<?php


namespace MotorCheck\App\Entities;

/**
 * @Entity
 * @Table(name="reposiroty")
 */
class Repository
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /**
     * @ManyToOne(targetEntity="Owner", inversedBy="repositories")
     */
    private $owner;

    /** @Column(type="string", nullable=false) */
    private $name;

    /** @Column(type="integer")*/
    private $watchers;

    /** @Column(type="integer")*/
    private $forks;

    /** @Column(type="integer")*/
    private $stars;

    /** @Column(type="string")*/
    private $url;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    /**
     * @param mixed $watchers
     */
    public function setWatchers($watchers): void
    {
        $this->watchers = $watchers;
    }

    /**
     * @return mixed
     */
    public function getForks()
    {
        return $this->forks;
    }

    /**
     * @param mixed $forks
     */
    public function setForks($forks): void
    {
        $this->forks = $forks;
    }

    /**
     * @return mixed
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @param mixed $stars
     */
    public function setStars($stars): void
    {
        $this->stars = $stars;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
}