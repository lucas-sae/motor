<?php


namespace MotorCheck\App\Controllers;


use League\Csv\Writer;
use MotorCheck\App\App;
use MotorCheck\App\Helpers\AttachmentResponse;
use SplTempFileObject;
use Zend\Diactoros\Response;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Stream;

class ReportController
{
    public function index()
    {
        $response = new HtmlResponse(App::twig()->render('report/index.html.twig'));

        return $response;
    }


    public function listReportData()
    {
        $data = $this->getData();

        $response = new JsonResponse(
            ['items' => $data],
            200,
            ['Content-Type' => ['application/hal+json']]
        );

        return $response;
    }

    public function downloadReport()
    {
        $data = $this->getData();

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Name', 'Date', 'Total Amount', 'Percent', 'Commission']);
        $csv->insertAll($data);

        $csv->output();

        $response = new AttachmentResponse($csv->output());

        return $response;
    }


    public function getData()
    {
        $pdo = App::pdo();
        $stmt = $pdo->prepare($this->getReportSql());

        $stmt->execute();

        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    private function getReportSql()
    {
        return "SELECT 
                    st.name,
                    st.created_at date,
                    ROUND(data.totalAmount, 2) totalAmount,
                    CASE
                        WHEN data.totalAmount > 2000000 AND ROUND(datediff(now(), st.created_at) / 360) > 1 THEN 17
                        WHEN data.totalAmount > 1000000 AND ROUND(datediff(now(), st.created_at) / 360) > 2 THEN 15
                        WHEN ROUND(datediff(now(), st.created_at) / 360) > 2 THEN 15
                        WHEN data.totalAmount > 1000000 AND ROUND(datediff(now(), st.created_at) / 360) <= 1 THEN 11
                        WHEN data.totalAmount > 200000 THEN 8
                        ELSE 7
                    END percent,
                    CASE
                        WHEN data.totalAmount > 2000000 AND ROUND(datediff(now(), st.created_at) / 360) > 1 THEN ROUND((17 * data.totalAmount) / 100, 2)
                        WHEN data.totalAmount > 1000000 AND ROUND(datediff(now(), st.created_at) / 360) > 2 THEN ROUND((15 * data.totalAmount) / 100, 2)
                        WHEN ROUND(datediff(now(), st.created_at) / 360) > 2 THEN ROUND((15 * data.totalAmount) / 100, 2)
                        WHEN data.totalAmount > 1000000 AND ROUND(datediff(now(), st.created_at) / 360) <= 1 THEN ROUND((11 * data.totalAmount) / 100, 2)
                        WHEN data.totalAmount > 200000 THEN ROUND((8 * data.totalAmount) / 100, 2)
                        ELSE ROUND((7 * data.totalAmount) / 100, 2)
                    END commission
                FROM
                    sales_team st
                        LEFT JOIN (SELECT name, SUM(quantity * unitary_value) totalAmount FROM sales_records GROUP BY name) data ON st.name = data.name";
    }
}