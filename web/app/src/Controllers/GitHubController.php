<?php


namespace MotorCheck\App\Controllers;

use Doctrine\ORM\EntityManager;
use League\Csv\Writer;
use MotorCheck\App\App;
use MotorCheck\App\Entities\Owner;
use MotorCheck\App\Entities\Repository;
use MotorCheck\App\Helpers\AttachmentResponse;
use SplTempFileObject;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class GitHubController
{
    public function index()
    {
        $em =  App::em();
        $owners = $em->getRepository(Owner::class)->findAll();
        if (count($owners) === 0) {
            $laravel = new Owner();
            $laravel->setName('laravel');
            $symfony = new Owner();
            $symfony->setName('symfony');

            $em->persist($laravel);
            $em->persist($symfony);
            $em->flush();
        }

        $response = new Response\HtmlResponse(App::twig()->render('github/index.html.twig'));

        return $response;
    }

    public function listRepositories()
    {
        $repositories = App::em()->getRepository(Repository::class)->findAll();

        $items = [];

        /**
         * @var Repository[] $repositories
         */
        foreach ($repositories as $repository) {
            $items[] = [
                'ownerName' => $repository->getOwner()->getName(),
                'repositoryName' => $repository->getName(),
                'watchers' => $repository->getWatchers(),
                'forks' => $repository->getForks(),
                'stars' => $repository->getStars(),
                'url' => $repository->getUrl()
            ];
        }

        $response = new Response\JsonResponse(
            ['items' => $items],
            200,
            ['Content-Type' => ['application/hal+json']]
        );

        return $response;
    }

    public function fetchFromApi()
    {
        /**
         * @var EntityManager $em
         */
        $em = App::em();

        $owners = $em->getRepository(Owner::class)->findAll();

        foreach ($owners as $owner) {
            $page = 1;
            do {
                $response = App::client()->get($owner->getName() . '/repos?page=' . $page);
                foreach (json_decode($response->getBody()->getContents()) as $repository) {
                    /** @var Repository $repo */
                    $repo = new Repository();

                    $repo->setName($repository->name);
                    $repo->setOwner($owner);
                    $repo->setWatchers($repository->watchers);
                    $repo->setForks($repository->forks);
                    $repo->setStars($repository->stargazers_count);
                    $repo->setUrl($repository->html_url);

                    $em->persist($repo);
                }
                $page++;
            } while (strlen($response->getBody()->getContents()) > 5);

            $em->flush();
        }

        $response = new Response\JsonResponse(
            ['message' => 'Data Updated'],
            200,
            ['Content-Type' => ['application/hal+json']]
        );

        return $response;
    }

    public function exportToCsv()
    {
        $pdo = App::pdo();

        $stmt = $pdo->prepare($this->githubQuery());
        $stmt->execute();

        $report = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Organization', 'Repository', 'Stars']);
        $csv->insertAll($report);

        $response = new AttachmentResponse($csv->output());

        return $response;
    }

    private function githubQuery()
    {
        return "SELECT 
                    o.name organization, r.name repository, r.stars
                FROM
                    reposiroty r
                        INNER JOIN owner o ON r.owner_id = o.id
                WHERE
                    r.watchers >= 100
                    AND r.stars > 2000
                ORDER BY r.stars DESC
                LIMIT 10";
    }
}