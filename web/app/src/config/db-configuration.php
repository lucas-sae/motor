<?php

return [
    'host' => getenv('DB_HOSTNAME'),
    'dbname' => getenv('DB_DATABASE'),
    'user' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'driver' => 'pdo_mysql',
];